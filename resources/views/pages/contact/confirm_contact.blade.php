@extends('layouts.app')


@section('content')

    <div class="container mt-4">
        <h1>Contact Confirmation</h1>
        <table class="table table-bordered" style="margin-top: 2%">
            <tr>
                <th scope="row">Name</th>
                <td>{{$name}}</td>
            </tr>

            <tr>
                <th scope="row">Email</th>
                <td>{{$email}}</td>
            </tr>

            <tr>
                <th scope="row">Phone Number</th>
                <td>{{$phone}}</td>
            </tr>

            <tr>
                <th scope="row">Subject</th>
                <td>{{$subject}}</td>
            </tr>

            <tr>
                <th scope="row" style="padding-bottom: 10%;">Message</th>
                <td><p>{{$message}}</p></td>
            </tr>

        </table>
        <div style="margin-top: 5%">
            <a href="/contact"><button type="button" class="btn btn-outline-dark" style="float: left;">Back</button>
                
            <a href="/contact/success">  <button type="button" class="btn btn-outline-dark" style="float: right;">Summit</button></a>

        </div>
    </div>

@endsection
