@extends('layouts.app')


@section('content')

    <div class="container mt-4">

        <h1>Contact Success</h1>

        <div class="text-center" style="margin-top: 25%">
            <h2>You message have been submit to our system!</h2>
            <h4>Our team will feedback to you soon, Thank you</h4>
        </div>


        <div class="text-center" style="margin-top: 10%">
            <a href="{{url('/')}}"><button type="button" class="btn btn-outline-dark" style="float: center;">GO BACK HOME PAGE</button></a>
        </div>


    </div>

@endsection
