@extends('layouts.app')


@section('content')

    <div class="container mt-5">
        <h1>Contact List</h1>
        <br>
        <form >
            <div class="form-group">
                <label for="inputName">Name</label>
                <input type="text" class="form-control" id="inputName" placeholder="Name">
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Email</label>
                    <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputPhoneNumber">Phone Number</label>
                    <input type="text" class="form-control" id="inputPhoneNumber" placeholder="Phone Number">
                </div>
            </div>
            <div class="form-group">
                <label for="inputSubject">Subject</label>
                <input type="text" class="form-control" id="inputAddress2" placeholder="">
            </div>
            <div class="form-group">
                <label for="inputMessage">Message</label>
                <textarea class="form-control" id="Message" style="min-height: 128px;">

          </textarea>
            </div>

            <a href="/contact/confirm"><button type="button" class="btn btn-outline-primary  btn-default" style="float: right;">Next</button></a>
        </form>
    </div>
@endsection
