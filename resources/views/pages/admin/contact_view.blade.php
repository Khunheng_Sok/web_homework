@extends('layouts.app')


@section('content')

    <div class="container mt-4">

        <h1>Contact Confirmation</h1>
        <hr>
        <div class="ml-auto float-right pb-3">
            <button class="btn btn-outline-dark">Pending</button>
            <button class="btn btn-outline-dark">In progress</button>
            <button class="btn btn-outline-dark">Completed</button>
        </div>
        <table class="table table-bordered" style="margin-top: 2%">
            <tr>
                <th scope="row">Name</th>
                <td>{{$name}}</td>
            </tr>

            <tr>
                <th scope="row">Email</th>
                <td>{{$email}}</td>
            </tr>

            <tr>
                <th scope="row">Phone Number</th>
                <td>{{$phone}}</td>
            </tr>

            <tr>
                <th scope="row">Subject</th>
                <td>{{$subject}}</td>
            </tr>

            <tr>
                <th scope="row" style="padding-bottom: 10%;">Message</th>
                <td><p>{{$message}}</p></td>
            </tr>

        </table>
        <div style="margin-top: 5%">
            
            <a href="{{url('admin/contacts')}}"><button type="button" class="btn btn-outline-secondary">< BACK</button></a>
            <button class="btn btn-outline-secondary" type="button" id="email" data-toggle="modal" data-target="#myModal" style="float: right">Reply</button>
            
            <!-- Modal -->
            <div class="modal fade" id="myModal">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h6>EMAILS</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="to">To</label>
                                <input type="text" class="form-control" id="to">
                            </div>
                            <div class="form-group">
                                <label for="subject">Subject</label>
                                <input type="text" class="form-control" id="subject">
                            </div>
                            <div class="form-group">
                                <label for="message">Message</label>
                                <textarea class="form-control" rows="8" id="message"></textarea>
                            </div>
                            <div class="form-inline justify-content-between">
                                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">CLOSE</button>
                                <a href="{{url('admin/contacts')}}"><button type="button" class="btn btn-outline-secondary">SEND</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
