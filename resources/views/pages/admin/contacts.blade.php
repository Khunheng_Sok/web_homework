@extends('layouts.app')


@section('content')

    @php
        $times = [1,2,3,4,5,6,7,8]
    @endphp
    <div class="container">

        <h1>Contact List</h1>
        <hr>
        <div class="container d-flex justify-content-end">
            <button class="btn btn-outline-secondary mb-4" type="button" id="email" data-toggle="modal" data-target="#myModal"><span>BULK EMAILS </span><i class="fa fa-envelope-o"></i></button>
            
                <!-- Modal -->
                <div class="modal fade" id="myModal">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h6>BULK EMAILS</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="to">To</label>
                                    <input type="text" class="form-control" id="to">
                                </div>
                                <div class="form-group">
                                    <label for="subject">Subject</label>
                                    <input type="text" class="form-control" id="subject">
                                </div>
                                <div class="form-group">
                                    <label for="message">Message</label>
                                    <textarea class="form-control" rows="8" id="message"></textarea>
                                </div>
                                <div class="form-inline justify-content-between">
                                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">CLOSE</button>
                                    <a href="{{url('admin/contacts/{id}')}}"><button type="button" class="btn btn-outline-secondary">SEND</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Modal -->
        </div>

        <div class="row">
            <div class="container">

                <div class="btn-group" style="margin-left: 0.5%; margin-bottom: 2%;">
                    <button type="button" class="btn border-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                        Bulk Action
                    </button>

                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">1 action</a>
                        <a class="dropdown-item" href="#">2 action</a>
                    </div>
                </div>
                


                <div class="btn-group" style="margin-bottom: 2%">
                    <button type="button" class="btn border border-info " style="float: left; margin-bottom: 2%;">Apply</button>

                </div>
                <div class="btn-group" style="margin-left: 0.5%; margin-bottom: 2%;">
                    <button type="button" class="btn border-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                        Status
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">1 action</a>
                        <a class="dropdown-item" href="#">2 action</a>
                    </div>
                </div>

                <button type="button" class="btn border border-info" style="margin-bottom: 2%;">Filter</button>
                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 2%; margin-left: 2%">Search</button>

                <div class="form-group" style="float: right;">
                    <input type="text" class="form-control" id="inputKey" aria-describedby="key" placeholder="Enter Keyword">

                </div>


            </div>
        </div>


       


        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">#No</th>
                <th scope="col">Name</th>
                <th scope="col">Subject</th>
                <th scope="col">Email</th>
                <th scope="col">Status</th>
                <th scope="col">Date</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($times as $time)
                <tr>
                    <th scope="row">#1</th>
                    <td>{{$name}}</td>
                    <td>{{$subject}}</td>
                    <td>{{$email}}</td>
                    <td>{{$status}}</td>
                    <td>{{$date}}</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn border border-secondary">Edit</button>
                            <button type="button" class="btn border border-secondary">Delete</button>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <p style="float: left; margin-top:2%;"> Total item: 50</p>

        <nav aria-label="list-of-message" style="float: right; margin-top:2%;">
            <ul class="pagination">
                <li class="page-item m-1"><a class="page-link" href="#"><</a></li>
                <li class="page-item m-1"><a class="page-link" href="#">1</a></li>
                <li class="page-item m-1"><a class="page-link" href="#">2</a></li>
                <li class="page-item m-1"><a class="page-link" href="#">3</a></li>
                <li class="page-item m-1"><a class="page-link" href="#">></a></li>
            </ul>
        </nav>
    </div>

@endsection
