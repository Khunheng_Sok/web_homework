@extends('layouts.app')


@section('content')

<div class="container">
    <h1>Contact Confirmation</h1>
    <hr>
    <table class="table table-bordered" style="margin-top: 5%">
          <tr>
            <th scope="row">Name</th>
            <td>Chris Salvotour</td>
          </tr>

          <tr>
            <th scope="row">Email</th>
            <td>khunhengsok@gmail.com</td>
          </tr>

          <tr>
          <th scope="row">Phone Number</th>
            <td>45987654</td>
          </tr>

          <tr>
            <th scope="row">Subject</th>
            <td>Supreme</td>
          </tr>

          <tr>
            <th scope="row" style="padding-bottom: 10%;">Message</th>
            <td><p>Consectetuer, penatibus. Sagittis sequi quidem sit hic mauris? Officia deleniti repellat fugiat
                sem voluptatibus! Nihil. Adipisicing! Aptent interdum eaque veniam reprehenderit ab aliqua
                dictumst, ornare quis, doloremque, commodi irure lorem laborum suscipit, mattis feugiat
                accusantium etiam dignissimos reiciendis officia autem ullamco deleniti eaque sunt irure
                accumsan rutrum lorem parturient phasellus, commodi magni. Dolore unde ipsum est quis, ac,

                dictumst elementum, eaque est dictumst cum! Nulla. Minim tempore bibendum laudantium
                placeat nemo dignissimos, inventore quaerat officia quisquam auctor nisl? Aliqua facilisis.
                Pharetra, ornare expedita error. Esse cras libero eget velit ullam, tristique eveniet diamlorem
                nullam quos! Nemo curae minim? Nemo class. Consectetuer, penatibus. Sagittis sequi quidem

                sit hic mauris? Officia deleniti repellat fugiat
                sem voluptatibus! Nihil. Adipisicing! Aptent interdum eaque veniam reprehenderit ab aliqua
                dictumst, ornare quis, doloremque, commodi irure lorem</p></td>
          </tr>

      </table>
      <div style="margin-top: 5%">
        <button type="button" class="btn btn-outline-dark" style="float: left;">Back</button>
        <button type="button" class="btn btn-outline-dark" style="float: right;">Summit</button>

      </div>
</div>

@endsection
