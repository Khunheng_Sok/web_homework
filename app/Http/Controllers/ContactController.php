<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.contact.contact');
        
    }

    public function contact_success(){
        return view('pages.contact.success');
    }


    public function confirm_contact(){
        return view('pages.contact.confirm_contact')->with([
            'name'=>"khunhengsok",
            'email'=>'khunhengsok@gmail.com',
            'phone'=>'012 34 56 78',
            'subject'=>'web',
            'message'=>'Consectetuer, penatibus. Sagittis sequi quidem sit hic mauris? Officia deleniti repellat fugiat
                sem voluptatibus! Nihil. Adipisicing! Aptent interdum eaque veniam reprehenderit ab aliqua
                dictumst, ornare quis, doloremque, commodi irure lorem laborum suscipit, mattis feugiat
                accusantium etiam dignissimos reiciendis officia autem ullamco deleniti eaque sunt irure
                accumsan rutrum lorem parturient phasellus, commodi magni. Dolore unde ipsum est quis, ac,

                dictumst elementum, eaque est dictumst cum! Nulla. Minim tempore bibendum laudantium
                placeat nemo dignissimos, inventore quaerat officia quisquam auctor nisl? Aliqua facilisis.
                Pharetra, ornare expedita error. Esse cras libero eget velit ullam, tristique eveniet diamlorem
                nullam quos! Nemo curae minim? Nemo class. Consectetuer, penatibus. Sagittis sequi quidem

                sit hic mauris? Officia deleniti repellat fugiat
                sem voluptatibus! Nihil. Adipisicing! Aptent interdum eaque veniam reprehenderit ab aliqua
                dictumst, ornare quis, doloremque, commodi irure lorem'
        ]);
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
