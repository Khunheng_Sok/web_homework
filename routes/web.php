<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/contact', function(){
    return view('pages.contact');
});




Route::get('/contact/confirm', "ContactController@confirm_contact");
Route::get('/contact/success', "ContactController@contact_success");

Route::resource('/contact', "ContactController");
Route::resource('/admin/contacts', "AdminContactController");

